﻿using System;

namespace HelloNpoi.RowParser
{
    public interface IExcelCellAccessor
    {
        string StringValue();
        DateTime DateTimeValue();
    }
}