﻿using System;
using NPOI.SS.UserModel;

namespace HelloNpoi.RowParser
{
    public class NpoiCellAccessor : IExcelCellAccessor
    {
        private readonly ICell _cell;

        public NpoiCellAccessor(ICell cell)
        {
            _cell = cell;
        }

        public string StringValue()
        {
            return _cell.StringCellValue;
        }

        public DateTime DateTimeValue()
        {
            return DateTime.FromOADate(_cell.NumericCellValue);
        }
    }
}