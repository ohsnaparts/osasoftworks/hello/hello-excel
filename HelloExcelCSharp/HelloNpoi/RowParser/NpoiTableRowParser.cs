﻿using System;
using System.Linq;
using HelloNpoi.Models;
using NPOI.SS.UserModel;

namespace HelloNpoi.RowParser
{
    public abstract class NpoiTableRowParser<TRow> : IRowParser<TRow> where TRow : ITableRow
    {
        protected IExcelCellAccessor FindCell(IRow r, ILookup<string, int> columns, string columnName)
        {
            if (!columns.Contains(columnName))
                return null;

            var index = columns[columnName].First();
            var cell = r.Cells.Find(c => c.ColumnIndex == index);

            if (cell == null)
                return null;

            var accessor = new NpoiCellAccessor(cell);
            return accessor;
        }

        public abstract TRow ParseRow(IRow row, ILookup<string, int> columns);
    }
}