﻿using System.Linq;
using HelloNpoi.Models;
using NPOI.SS.UserModel;

namespace HelloNpoi.RowParser
{
    public class ParameterTableRowParser : NpoiTableRowParser<ParameterTableRow>
    {
        /// <summary></summary>
        /// <param name="row"></param>
        /// <param name="columns">Column Dictionary with Key:ColumnName, Value: ColumnIndex</param>
        /// <returns></returns>
        public override ParameterTableRow ParseRow(IRow row, ILookup<string, int> columns)
        {
            IExcelCellAccessor FindCell(string columnName) => this.FindCell(row, columns, columnName);

            var parsedRow = new ParameterTableRow
            {
                RowIndex = row.RowNum,
                Activity = FindCell("Activities").StringValue() ?? "",
                Customer = FindCell("Customers").StringValue() ?? "",
                Project = FindCell("Projects")?.StringValue() ?? "",
                Skill = FindCell("Skills")?.StringValue() ?? ""
            };

            return parsedRow;
        }
    }
}