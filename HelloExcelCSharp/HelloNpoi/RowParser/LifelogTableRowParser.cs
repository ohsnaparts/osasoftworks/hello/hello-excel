﻿using System;
using System.Linq;
using HelloNpoi.Models;
using NPOI.SS.UserModel;

namespace HelloNpoi.RowParser
{
    public class LifelogTableRowParser : NpoiTableRowParser<LifelogTableRow>
    {
        public override LifelogTableRow ParseRow(IRow row, ILookup<string, int> columns)
        {
            IExcelCellAccessor FindCell(string columnName) => this.FindCell(row, columns, columnName);

            var parsedRow = new LifelogTableRow();
            try
            {
                parsedRow.RowIndex = row.RowNum;
                parsedRow.Customer = FindCell(nameof(LifelogTableRow.Customer))?.StringValue();
                parsedRow.Start = FindCell(nameof(LifelogTableRow.Start))?.DateTimeValue() ?? default;
                parsedRow.Stop = FindCell(nameof(LifelogTableRow.Stop))?.DateTimeValue() ?? default;
                parsedRow.Ref = FindCell(nameof(LifelogTableRow.Ref))?.StringValue();
                parsedRow.Story = FindCell(nameof(LifelogTableRow.Story))?.StringValue();
                parsedRow.Headline = FindCell(nameof(LifelogTableRow.Headline))?.StringValue();

                parsedRow.Project = FindCell(nameof(LifelogTableRow.Project))?.StringValue() ?? parsedRow.Customer;
                parsedRow.Activity = FindCell(nameof(LifelogTableRow.Activity))?.StringValue() ?? "Developing";
                parsedRow.Skill = FindCell(nameof(LifelogTableRow.Skill))?.StringValue() ?? parsedRow.Project;
            }
            catch (Exception ex)
            {
                throw new ParsingException(
                    $"An error occured while parsing row {row.RowNum} " +
                    $"of sheet {row.Sheet.SheetName}", ex
                )
                {
                    Data = {{"Row", row}}
                };
            }

            return parsedRow;
        }
    }
}