﻿using System;
using System.Linq;
using HelloNpoi.Models;
using NPOI.SS.UserModel;

namespace HelloNpoi.RowParser
{
    public interface IRowParser<TRow> where TRow : ITableRow
    {
        /// <summary></summary>
        /// <param name="row"></param>
        /// <param name="columns">Key: ColumnName, Value: ColumnIndex</param>
        /// <returns></returns>
        TRow ParseRow(IRow row, ILookup<string, int> columns);
    }
}