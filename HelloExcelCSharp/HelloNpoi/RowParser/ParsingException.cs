﻿using System;

namespace HelloNpoi.RowParser
{
    public class ParsingException : ApplicationException
    {
        public ParsingException(string message, Exception inner) : base(message, inner)
        {
        }

        public ParsingException(string message) : this(message, null)
        {
        }
    }
}