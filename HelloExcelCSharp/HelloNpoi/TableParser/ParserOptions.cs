﻿namespace HelloNpoi.TableParser
{
    public class ParserOptions : IParserOptions
    {
        public int StopAtRow { get; set; }
        public bool SkipEmpty { get; set; }
        public bool SkipInvalid { get; set; }
    }
}