﻿using System;
using System.Collections.Generic;
using System.Linq;
using HelloNpoi.Models;
using HelloNpoi.RowParser;
using HelloNpoi.Validator;
using NPOI.SS.UserModel;

namespace HelloNpoi.TableParser
{
    public abstract class NpoiTableParser<TRow> : ITableParser<TRow> where TRow : ITableRow
    {
        private readonly IRowParser<TRow> _rowParser;
        private readonly IValidator<TRow> _rowValidator;
        private readonly IParserOptions _parserOptions;


        public NpoiTableParser(
            IRowParser<TRow> rowParser,
            IValidator<TRow> rowValidator,
            IParserOptions parserOptions
        )
        {
            _rowParser = rowParser;
            _rowValidator = rowValidator;
            _parserOptions = parserOptions;
        }


        private List<IRow> ReadRows(ISheet sheet)
        {
            bool IsEmpty(IRow r) => r == null || r.Cells.All(c => c.CellType == CellType.Blank);

            var rows = new List<IRow>();
            foreach (IRow row in sheet)
            {
                if (_parserOptions?.StopAtRow == row.RowNum)
                    break;

                if ((_parserOptions?.SkipEmpty ?? false) && IsEmpty(row))
                    continue;

                rows.Add(row);
            }

            return rows;
        }


        public List<TRow> ParseWorksheet(ISheet sheet)
        {
            if (sheet == null)
                throw new ArgumentException("Unable to parser undefined worksheet.");

            var rows = ReadRows(sheet);
            if (rows.Count == 0)
                throw new ArgumentException("The sheet does not have any non-empty rows.", nameof(rows));


            var columns = rows.First().Cells.ToLookup(c =>
                {
                    c.SetCellType(CellType.String);
                    return c.StringCellValue;
                }, c => c.ColumnIndex
            );

            ;var list = rows
                .Skip(1)
                .Select(r => _rowParser.ParseRow(r, columns))
                .Where(r =>
                    !(_parserOptions?.SkipInvalid ?? false) || _rowValidator.TryValidate(r, out var reason)
                ).ToList();

            return list;
        }
    }
}