﻿namespace HelloNpoi.TableParser
{
    public interface IParserOptions
    {
        int StopAtRow { get; set; }
        bool SkipEmpty { get; set; }
        bool SkipInvalid { get; set; }
    }
}