﻿using HelloNpoi.Models;
using HelloNpoi.RowParser;
using HelloNpoi.Validator;

namespace HelloNpoi.TableParser
{
    public class LifelogTableParser : NpoiTableParser<LifelogTableRow>
    {
        public LifelogTableParser(
            IRowParser<LifelogTableRow> rowParser,
            IValidator<LifelogTableRow> rowValidator,
            IParserOptions parserOptions
        ) : base(rowParser, rowValidator, parserOptions)
        {
        }
    }
}