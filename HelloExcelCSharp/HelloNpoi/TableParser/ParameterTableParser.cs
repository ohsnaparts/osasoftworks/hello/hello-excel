﻿using HelloNpoi.Models;
using HelloNpoi.RowParser;

namespace HelloNpoi.TableParser
{
    public class ParameterTableParser : NpoiTableParser<ParameterTableRow>
    {
        public ParameterTableParser(IRowParser<ParameterTableRow> rowParser) 
            : base(rowParser, null, null)
        {
        }
    }
}