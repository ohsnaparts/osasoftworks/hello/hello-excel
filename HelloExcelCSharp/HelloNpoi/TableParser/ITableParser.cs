﻿using System.Collections.Generic;
using System.IO;
using HelloNpoi.Models;
using NPOI.SS.UserModel;

namespace HelloNpoi.TableParser
{
    public interface ITableParser<TRow> where TRow : ITableRow
    {
        List<TRow> ParseWorksheet(ISheet worksheet);
    }
}
