﻿namespace HelloNpoi.Models
{
    public class ParameterTableRow : ITableRow
    {
        public int RowIndex { get; set; }
        public string Customer { get; set; }
        public string Project { get; set; }
        public string Activity { get; set; }
        public string Skill { get; set; }


        public override string ToString()
        {
            return $"{RowIndex}: " + string.Join('|', new {
                Customer, Project, Activity, Skill
            });
        }
    }
}