﻿namespace HelloNpoi.Models
{
    public interface ITableRow
    {
        int RowIndex { get; set; }
    }
}