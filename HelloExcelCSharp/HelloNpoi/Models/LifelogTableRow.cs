﻿using System;

namespace HelloNpoi.Models
{
    public class LifelogTableRow : ITableRow
    {
        public int RowIndex { get; set; }
        public string Customer { get; set; }
        public string Project { get; set; }
        public DateTime Start { get; set; }
        public DateTime Stop { get; set; }
        public string Activity { get; set; }
        public string Skill { get; set; }
        public string Ref { get; set; }
        public string Story { get; set; }
        public string Headline { get; set; }

        public override string ToString() => $"{RowIndex}: " +
                                             $"{Customer}, " +
                                             $"{Project}, " +
                                             $"{Start}, " +
                                             $"{Stop}, " +
                                             $"{Activity} " +
                                             $"{Skill} " +
                                             $"{Ref} " +
                                             $"{Story} " +
                                             $"{Headline}";
    }
}