﻿using System;
using System.Drawing.Text;
using HelloNpoi.Models;

namespace HelloNpoi.Validator
{
    public interface IValidator<T>
    {
        bool TryValidate(T obj, out string reason);
    }


    public class LifelogTableRowValidator : IValidator<LifelogTableRow>
    {
        public bool TryValidate(LifelogTableRow obj, out string reason)
        {
            if (string.IsNullOrWhiteSpace(obj.Customer))
            {
                reason = $"Property {nameof(obj.Customer)} is empty.";
                return false;
            }

            if (string.IsNullOrWhiteSpace(obj.Project))
            {
                reason = $"Property {nameof(obj.Project)} is empty.";
                return false;
            }

            if (obj.Start == default && obj.Stop == default)
            {
                reason = $"Either {nameof(obj.Start)} or {nameof(obj.Stop)} date are undefined.";
                return false;
            }

            reason = "";
            return true;
        }
    }
}