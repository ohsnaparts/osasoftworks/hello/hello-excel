﻿
$Script:ParserExec = 'C:\Workspace\hello-space\hello-excel\C#\Hello-NPOI\HelloExcelCSharp\bin\Debug\netcoreapp2.2\HelloExcelCSharp.dll'

Function Validate-Sheets {
    PARAM(
        [string] $SheetFolderPath,
        [string[]] $Worksheets
    )

    Get-ChildItem $SheetFolderPath -File -Filter '*.xlsx' | ForEach-Object {
        Validate-Sheet -ExcelFilePath $_.FullName -Worksheet $Worksheets
    }
}


Function Validate-Sheet {
    [CmdletBinding()]
    PARAM(
        [string] $ExcelFilePath,
        [string[]] $Worksheets
    )

    $File = Get-Item -Path $ExcelFilePath
    [HashTable] $ObjHash = @{
        Name = $File.Name
        File = $File.FullName
        Worksheets = $Worksheets

    };

    $ObjHash["Output"] = dotnet "$Script:ParserExec" "$ExcelFilePath" $Worksheets *>&1
    $ObjHash["Valid"] = $?

    $Result = [psobject]$ObjHash
    Write-Host -NoNewline "$($Result.Name) - $($Result.Worksheets): "
    Write-Host $Result.Valid -BackgroundColor $(If($Result.Valid) { "Green" } Else { "Red" })
    Write-Output $Result
}


$Worksheets = (
    'Monday',
    'Thuesday',
    'Wednesday',
    'Thursday',
    'Friday',
    'Saturday',
    'Sunday'
)


$WorksheetFolderPath = 'C:\Users\kami\OneDrive - Zuehlke\Timetracking'

$Results = Get-ChildItem $WorksheetFolderPath -Filter *.xlsx -File `
         | Select-Object `
         | ForEach-Object {
    $Path = $_.FullName
    $Name = $_.Name

        Write-Progress -Activity "Validating $Name" -CurrentOperation $_

        Validate-Sheet -ExcelFilePath "$Path"  `
                        -Worksheets @($Worksheets)
}

$results