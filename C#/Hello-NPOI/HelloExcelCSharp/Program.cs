﻿using System;
using System.IO;
using System.Linq;
using HelloNpoi.RowParser;
using HelloNpoi.TableParser;
using HelloNpoi.Validator;
using NPOI.SS.UserModel;
using NPOI.XSSF.UserModel;

namespace HelloExcelCSharp
{
    class Program
    {

        static void Main(string[] args)
        {
            if (args == null || !args.Any())
                throw new ArgumentException(
                    "First argument must be a an excel (.xlsx) file path.");

            var filePath = args[0];
            if (!File.Exists(filePath))
                throw new ArgumentException($"File {filePath} not found.", nameof(filePath));

            var worksheets = args.Skip(1)
                .Where(arg => !string.IsNullOrWhiteSpace(arg))
                .ToList();

            if (!worksheets.Any())
                throw new ArgumentException("No worksheets were specified to parse.");

            //var rowParser = new ParameterTableRowParser();
            //var tableParser = new ParameterTableParser(rowParser);

            var tableParser = new LifelogTableParser(
                new LifelogTableRowParser(),
                new LifelogTableRowValidator(),
                new ParserOptions {
                    StopAtRow = 29,
                    SkipEmpty = true,
                    SkipInvalid = true
                }
            );

            IWorkbook workbook;
            using (var fileStream = File.OpenRead(filePath))
            {
                workbook = OpenWorkbook(fileStream);
            }

            worksheets.ForEach(worksheet =>
            {
                var sheet = workbook.GetSheet(worksheet);
                var entries = tableParser.ParseWorksheet(sheet);
                
                var skills = entries
                    .Select(e => e.Skill)
                    .ToArray();

                Console.WriteLine(String.Join(',', skills));
            });


            //Console.ReadKey();
        }


        private static IWorkbook OpenWorkbook(Stream fileStream)
        {
            var workbook = new XSSFWorkbook(fileStream);

            if (workbook.NumberOfSheets == 0)
                throw new ApplicationException("Workbook does not have any sheets.");

            return workbook;
        }
    }
}